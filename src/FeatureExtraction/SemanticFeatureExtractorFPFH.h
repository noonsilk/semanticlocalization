/*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#ifndef _SEMANTICFEATUREEXTRACTORFPFH_H_
#define _SEMANTICFEATUREEXTRACTORFPFH_H_

#include "SemanticFeatureExtractor.h"
/** \brief FastPFH extractor class.
    *
    * The SemanticFeatureExtractorFPFH class is a interface of the PCL
	* FPFHEstimation method. Abstracts the input/output of this feature
	* and converts its internal representation to a std::vector<float*> format.
    * 
    */
class SemanticFeatureExtractorFPFH : public SemanticFeatureExtractor
{
public:
	SemanticFeatureExtractorFPFH();
	
	/** Loads the features from the pcd format to a std::vector<float*>.
    */
	void loadFeatures(std::string path, std::vector<float*> &features);
	
	/** Saves the features from the std::vector<float*> format to a pcd.
    */
	void saveFeatures(std::string path, std::vector<float*> features);
	
	/** \brief Extracts the features from the given keypoints.
	* Extract  the features from the given keypoints and source pointcloud
	* and return them in a std::vector<float*> where each position of the vector is 
	* one descriptor in a float * format.
    */
	void extractFeatures(const PointCloud<PointXYZRGB>::Ptr &src,
		const PointCloud<PointXYZRGB>::Ptr &keypoints,
		std::vector<float*> &features);

	~SemanticFeatureExtractorFPFH();
};

#endif
