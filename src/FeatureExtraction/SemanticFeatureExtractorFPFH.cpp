/*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include <pcl/features/fpfh.h>

#include "SemanticFeatureExtractorFPFH.h"
#include "FeatureExtractionCommon.h"

SemanticFeatureExtractorFPFH::SemanticFeatureExtractorFPFH()
{
	name = "FPFH";
	dimensionality = FPFHSignature33::descriptorSize();
}

void SemanticFeatureExtractorFPFH::loadFeatures(std::string file, std::vector<float*> &features)
{
	PointCloud<pcl::FPFHSignature33>::Ptr f_src(new PointCloud<pcl::FPFHSignature33>);
	//load the pointcloud of features
	pcl::io::loadPCDFile(file, *f_src);
	//reserve space for the features
	features.clear();
	features.reserve(f_src->size());
	//convert to vector<float*>
	for (int i = 0; i<f_src->size(); i++){
		float *desc = new float[dimensionality];
		for (int j = 0; j < dimensionality; j++)
		{
			desc[j] = f_src->points[i].histogram[j];
		}
		features.push_back(desc);
	}
}
void SemanticFeatureExtractorFPFH::saveFeatures(std::string file, std::vector<float*> features)
{
	PointCloud<pcl::FPFHSignature33>::Ptr f_src(new PointCloud<pcl::FPFHSignature33>);
	//reserve space for the features
	f_src->resize(features.size());
	//convert to pointcloud of pcl::feature
	for (int i = 0; i<features.size(); i++){
		for (int j = 0; j < dimensionality; j++)
		{
			f_src->points[i].histogram[j] = features.at(i)[j];
		}
	}
	//save the pointcloud of features
	pcl::io::savePCDFile(file, *f_src, true);
}


void SemanticFeatureExtractorFPFH::extractFeatures(const PointCloud<PointXYZRGB>::Ptr &src,
	const PointCloud<PointXYZRGB>::Ptr &keypoints,
	std::vector<float*> &features)
{
	PointCloud<pcl::FPFHSignature33>::Ptr f_src(new PointCloud<pcl::FPFHSignature33>);
	PointCloud<Normal>::Ptr normals_src(new PointCloud<Normal>);
	//Normal estimation
	estimateNormals(src, *normals_src);

	//compute the features
	FPFHEstimation<PointXYZRGB, Normal, FPFHSignature33> fpfh_est;

	if (src->isOrganized()){
		pcl::search::OrganizedNeighbor<PointXYZRGB>::Ptr on(new pcl::search::OrganizedNeighbor<PointXYZRGB>());
		fpfh_est.setSearchMethod(on);
	}
	else{
		pcl::search::KdTree<PointXYZRGB>::Ptr tree(new pcl::search::KdTree<PointXYZRGB>());
		fpfh_est.setSearchMethod(tree);
	}
	fpfh_est.setInputCloud(keypoints);
	fpfh_est.setInputNormals(normals_src);
	//Sets the K neighbors parameter to estimate the feature
	fpfh_est.setKSearch(50);
	fpfh_est.setSearchSurface(src);
	fpfh_est.compute(*f_src);
	
	features.reserve(f_src->size());

	//convert to vector<float*>
	for (int i = 0; i<f_src->size(); i++){
		float v = f_src->points[i].histogram[0];
		if (!pcl_isfinite(v)){
			f_src->points.erase(f_src->points.begin() + i);
			i--;
		}
		else{
			float *desc = new float[dimensionality];
			for (int j = 0; j < dimensionality; j++)
			{
				desc[j] = f_src->points[i].histogram[j];
			}
			features.push_back(desc);
		}
	}
	normals_src->clear();
	f_src->clear();
}

SemanticFeatureExtractorFPFH::~SemanticFeatureExtractorFPFH()
{
}
