/*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include <pcl/features/shot.h>

#include "FeatureExtractionCommon.h"
#include "SemanticFeatureExtractorColorSHOT.h"

SemanticFeatureExtractorColorSHOT::SemanticFeatureExtractorColorSHOT()
{
	name = "ColorSHOT";
	dimensionality = SHOT1344::descriptorSize();
}

void SemanticFeatureExtractorColorSHOT::loadFeatures(std::string file, std::vector<float*> &features)
{
	//load the pointcloud of features
	PointCloud<pcl::SHOT1344>::Ptr f_src(new PointCloud<pcl::SHOT1344>);
	pcl::io::loadPCDFile(file, *f_src);
	//reserve space for the features
	features.clear();
	features.reserve(f_src->size());
	//convert to vector<float*>
	for (int i = 0; i<f_src->size(); i++){
		float *desc = new float[dimensionality];
		for (int j = 0; j < dimensionality; j++)
		{
			desc[j] = f_src->points[i].descriptor[j];
		}
		features.push_back(desc);
	}
}

void SemanticFeatureExtractorColorSHOT::saveFeatures(std::string file, std::vector<float*> features)
{
	PointCloud<pcl::SHOT1344>::Ptr f_src(new PointCloud<pcl::SHOT1344>);
	//reserve space for the features
	f_src->resize(features.size());
	//convert to pointcloud of pcl::feature
	for (int i = 0; i<features.size(); i++){
		for (int j = 0; j < dimensionality; j++)
		{
			f_src->points[i].descriptor[j] = features.at(i)[j];
		}
	}
	//save the pointcloud of features
	pcl::io::savePCDFile(file, *f_src,true);
}


void SemanticFeatureExtractorColorSHOT::extractFeatures(const PointCloud<PointXYZRGB>::Ptr &src,
	const PointCloud<PointXYZRGB>::Ptr &keypoints,
	std::vector<float*> &features)
{

	PointCloud<pcl::SHOT1344>::Ptr f_src(new PointCloud<pcl::SHOT1344>);
	PointCloud<Normal>::Ptr normals_src(new PointCloud<Normal>);
	//Normal estimation
	estimateNormals(src, *normals_src);

	//compute the features
	pcl::SHOTColorEstimation<PointXYZRGB, Normal> shotestimator;
	shotestimator.setInputCloud(keypoints);
	shotestimator.setInputNormals(normals_src);
	//computes the pointcloud resolution
	float resolution = static_cast<float> (computeCloudResolution(src));
	//sets the radius to three times the resolution
	shotestimator.setRadiusSearch(3 * resolution);
	shotestimator.setSearchSurface(src);
	shotestimator.compute(*f_src);
	
	features.reserve(f_src->size());
	//convert to vector<float*>
	for (int i = 0; i<f_src->size(); i++){
		float v = f_src->points[i].descriptor[0];
		if (!pcl_isfinite(v)){
			f_src->points.erase(f_src->points.begin() + i);
			i--;
		}
		else{
			float *desc = new float[dimensionality];
			for (int j = 0; j < dimensionality; j++)
			{
				desc[j] = f_src->points[i].descriptor[j];
			}
			features.push_back(desc);
		}

	}
	normals_src->clear();
	f_src->clear();
}

SemanticFeatureExtractorColorSHOT::~SemanticFeatureExtractorColorSHOT()
{
}
