 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "SemanticLocalizationBoWSVM.h"
#include <float.h>

namespace fs = ::boost::filesystem;
using namespace std;

SemanticLocalizationBoWSVM::SemanticLocalizationBoWSVM()
{	
	// set default parameters

	svmParameters.svm_type = C_SVC;
	svmParameters.kernel_type = LINEAR;
	svmParameters.degree = 3;
	svmParameters.gamma = 0;
	
	svmParameters.coef0 = 0;
	svmParameters.nu = 0.5;
	svmParameters.cache_size = 100;
	svmParameters.C = 1;
	svmParameters.eps = 1e-3;
	svmParameters.p = 0.1;
	svmParameters.shrinking = 1;
	svmParameters.probability = 0;
	svmParameters.nr_weight = 0;
	svmParameters.weight_label = NULL;
	svmParameters.weight = NULL;

}

#define VERBOSE

static double chi2dist(const svm_node *px, const svm_node *py);


/*
*	Chi2 functions
*
*/

double
expchi2kernel (const Eigen::ArrayXd &x, const Eigen::ArrayXd &y, const float gamma = 1)
{
	Eigen::ArrayXd den = x - y;
	Eigen::ArrayXd num = x + y;
	double d = ((den * den) / (num +  std::numeric_limits<double>::epsilon ())).sum ();
	d = exp (-gamma * d);
	return d;
}

void
generatePrecomputedK (const std::vector<float*> &x, const std::vector<float*> &y, const int &feat_size, svm_node ***k)
{
	/*
	
	We use the node structure from libSVM

		struct svm_node
		{
		int index;
		double value;
		};

	In this structure, we use index values starting with 1.
	The end of the structure is set with index = -1

	Therefore, for an instance with dimmensionality dim we need to create a svm_node 
	with size dim+1 where the index of the element svm_node[dim]=-1	
	
	*/

	*k = new svm_node *[y.size ()];
	for(unsigned int j = 0; j < y.size (); j++)
	{
		float *y_ptr = y[j];
		Eigen::ArrayXf y_element = Eigen::Map<Eigen::ArrayXf> (y_ptr, feat_size);

		(*k)[j] = new svm_node[x.size () + 1];
		
		for(unsigned int i = 0; i < x.size (); i++)
		{
			float *x_ptr = x[i];
			Eigen::ArrayXf x_element = Eigen::Map<Eigen::ArrayXf> (x_ptr, feat_size);;
		
			(*k)[j][i].index = i + 1;
			(*k)[j][i].value = expchi2kernel(x_element.cast<double> (), y_element.cast<double> ());
		}
		
		(*k)[j][x.size ()].index = -1;
	}
}

void
generatePrecomputedKSingleTestInstance (const std::vector<float*> &x, float* y_ptr, const int &feat_size, svm_node *k)
{
	/*
	
	We use the node structure from libSVM

		struct svm_node
		{
		int index;
		double value;
		};

	In this structure, we use index values starting with 1.
	The end of the structure is set with index = -1

	Therefore, for an instance with dimmensionality dim we need to create a svm_node 
	with size dim+1 where the index of the element svm_node[dim]=-1	
	
	*/

	Eigen::ArrayXf y_element = Eigen::Map<Eigen::ArrayXf> (y_ptr, feat_size);

	k = new svm_node[x.size () + 1];
	
	for(unsigned int i = 0; i < x.size (); i++)
	{
		float *x_ptr = x[i];
		Eigen::ArrayXf x_element = Eigen::Map<Eigen::ArrayXf> (x_ptr, feat_size);;
	
		k[i].index = i + 1;
		k[i].value = expchi2kernel(x_element.cast<double> (), y_element.cast<double> ());
	}
	
	k[x.size ()].index = -1;
}


int SemanticLocalizationBoWSVM::classifyFrameScene(std::vector<float*> &features)
{
	std::vector<float*> testWords;
	int sceneLabel = 0;
	testWords.push_back(wordsAssignation(features));

	float* testWordsSingle = wordsAssignation(features);

	svm_node* testnode;
	
	if(svmParameters.kernel_type != PRECOMPUTED)
	{
		testnode = new svm_node[dictionarySize+1];
		for(int j=0;j<dictionarySize;j++)
			{	
			testnode[j].index = j+1;
			testnode[j].value = testWords.at(0)[j];
			}

		testnode[dictionarySize].index=-1;
	}
	else
	{
		  generatePrecomputedKSingleTestInstance(trainingWords, testWordsSingle, dictionarySize, testnode);	
	}

	sceneLabel = svm_predict(svmModel,testnode);	
	
	return sceneLabel;
}


void SemanticLocalizationBoWSVM::train()
{
	string fileName = "training_"+detector->name + "_" + extractor->name + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".csv";
	if(!boost::filesystem::exists(fileName))
		generateTrainingWords(true);
	else
		{

		std::cout << "Loading training words ..."<< std::endl;
		loadCSVFile(trainingWords, trainingClasses, fileName);

		// Regardless the training words, we need a dictionary to assign the features extracted from test files to test words
		dictionary.resize(0);

		string dictionary_name;
		dictionary_name = "dictionary_" + detector->name + "_" + extractor->name + + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".pcd";

		if (!boost::filesystem::exists(dictionary_name))	
			generateDictionaryFromTraining();		// The dictionary does not exist --> generate and save
		else
			extractor->loadFeatures(dictionary_name, dictionary);

		// Each centroid of the dictionary corresponds with a local feature, then we use the same method for load/save the dictionary

		}
	
	/*
	* Parameters:
	*	- int dictionarySize (dictionary size)
	*	- std::vector<float*> trainingWords;    training Words computed --> global var
	*	- std::vector<int> trainingClasses;	values for the training classes --> global var
	*/

	std::cout << "Starting train SVM classifier..."<< std::endl;
	svmModel = trainSVMClassifier(trainingWords, trainingClasses, dictionarySize);
	std::cout << "...end of train SVM classifier..."<< std::endl;

	return;
}

SemanticLocalizationBoWSVM::svm_model_ptr 
SemanticLocalizationBoWSVM::trainSVMClassifier(std::vector<float*> &trainingWords, std::vector<int> &trainingClasses, int dictionarySize)
{
	std::cout << endl << "TRAINING A SVM OF TYPE " <<  svmParameters.kernel_type <<  " WITH DEGREE: " <<  svmParameters.degree << " AND GAMMA: " << svmParameters.gamma << endl;
	bool saveCSVFile = false;
	bool saveModelFile = false;

	if(saveCSVFile)
		{
		ofstream csvFile;
		string fileName = "training_"+detector->name + "_" + extractor->name + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".csv";
		csvFile.open (fileName.c_str());

		for(int i=0;i<trainingWords.size();i++)
			{	
			for(int j=0;j<dictionarySize;j++)
				{	
				csvFile << trainingWords.at(i)[j] << ",";				
				}
			csvFile << trainingClasses.at(i) << "\n";
		}
		csvFile.close();
		}

	// set problem

	struct svm_problem problemBoW;
	struct svm_model *modelBoW;

	problemBoW.l = trainingWords.size();

	problemBoW.y = new double[problemBoW.l];

	std::cout << endl << "TRAINING WORDS: " << endl;

	/*
	
	We use the node structure from libSVM

		struct svm_node
		{
		int index;
		double value;
		};

	In this structure, we use index values starting with 1.
	The end of the structure is set with index = -1

	Therefore, for an instance with dimmensionality dim we need to create a svm_node 
	with size dim+1 where the index of the element svm_node[dim]=-1	
	
	*/


	if(svmParameters.kernel_type != PRECOMPUTED)	
	{
		svm_node** x_bow = new svm_node *[problemBoW.l];	
		for(int i=0;i<trainingWords.size();i++)
		{
			x_bow[i]=new svm_node[dictionarySize+1];
			for(int j=0;j<dictionarySize;j++)
				{	
				x_bow[i][j].index = j+1;
				x_bow[i][j].value = trainingWords.at(i)[j];
				}
			x_bow[i][dictionarySize].index = -1;
		
			problemBoW.y[i] = trainingClasses.at(i);
		}
		problemBoW.x = x_bow;
	}
	else
	{
		for(int i=0;i<trainingWords.size();i++)
			problemBoW.y[i] = trainingClasses.at(i);
		generatePrecomputedK(trainingWords, trainingWords, dictionarySize, &problemBoW.x);				
	}

	modelBoW = svm_train(&problemBoW, &svmParameters); 

	if(saveModelFile)
	{
		std::cout << endl << "Saving file ... " << endl;
		printf("Result:%d",svm_save_model("svmModel.txt",modelBoW));
	}

	return modelBoW;
}


SemanticLocalizationBoWSVM::~SemanticLocalizationBoWSVM()
{
	for (std::vector<float*>::iterator itb = trainingWords.begin(); itb != trainingWords.end(); ++itb)
		free(*itb);
	trainingWords.resize(0);
}

double chi2dist(const svm_node *px, const svm_node *py)
{
	double sum = 0;
	while(px->index != -1 && py->index != -1)
	{
		if(px->index == py->index)
		{
			double num = px->value - py->value;
			double denum = px->value + py->value;
			sum += num*num/(denum+DBL_MIN);
			++px;
			++py;
		}
		else
		{
			if(px->index > py->index){
				sum+=py->value;
				++py;
			}
			else{
				sum+=px->value;
				++px;
			}
		}			
	}
	
	while(px->index != -1)
	{
		sum += px->value;
		++px;
	}
	
	while(py->index != -1)
	{
		sum += py->value;
		++py;
	}
	
	return  sum;
}
