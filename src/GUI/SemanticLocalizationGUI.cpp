 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "SemanticLocalizationGUI.h"
#include "../build/ui_SemanticLocalizationGUI.h"


// GENERAL

#include <iostream>
#include <sys/types.h>
#include <dirent.h>

// BOOST

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>

// PCL

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/point_cloud_handlers.h>
#include <pcl/visualization/point_cloud_color_handlers.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/features/range_image_border_extractor.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/narf_descriptor.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/features/shot.h>

#include <pcl/segmentation/extract_clusters.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <pcl/console/parse.h>

#include <pcl/common/common_headers.h>

#include <pcl/range_image/range_image.h>

#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/keypoints/harris_3d.h>

#include <pcl/registration/correspondence_estimation.h>
#include <pcl/registration/correspondence_rejection_distance.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_one_to_one.h>
#include <pcl/registration/transformation_validation_euclidean.h>

// Semantic classification

//#include "semanticclasification.h"
#include "SemanticKeypointDetectorUniform.h"
#include "SemanticKeypointDetectorHarris3D.h"
#include "SemanticKeypointDetectorNARF.h"

#include "SemanticFeatureExtractorSHOT.h"
#include "SemanticFeatureExtractorColorSHOT.h"
#include "SemanticFeatureExtractorFPFH.h"
#include "SemanticFeatureExtractorPFHRGB.h"
#include "SemanticFeatureExtractorNARF.h"

// SemanticLocalization

#include "SemanticLocalization.h"
#include "SemanticLocalizationBoWSVM.h"

typedef pcl::PointXYZRGB PointType;
typedef pcl::PointXYZ KeyPointType;
typedef pcl::Normal NormalType;

namespace fs = ::boost::filesystem;
using namespace std;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;
using namespace pcl::registration;

// --------------------
// -----Parameters-----
// --------------------

float angular_resolution = 0.5f;
float angular_resolution_deg = 0.5f;
float angular_resolution_x = 0.5f,
      angular_resolution_y = angular_resolution_x;

float support_size = 0.2f;
pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
bool setUnseenToMaxRange = false;
bool rotation_invariant = true;
bool live_update = false;

bool firstTimeExtraction = true;
bool firstTimeExtractionLBP = true;
bool firstTimeLBP = true;

float normal_radius = 0.03f; // Radius for normals estimation (3cm)

SemanticLocalizationGUI::SemanticLocalizationGUI (QWidget *parent):QMainWindow (parent),ui(new Ui::SemanticLocalizationGUI)
	{

	// Initialize the value for the angular resolution
	angular_resolution = pcl::deg2rad (angular_resolution_deg);

	// Initialize the GUI and QT connections
	
	ui->setupUi (this);
	this->setWindowTitle ("Semantic Localization BoW Approach in PCL");

	QButtonGroup *bgKeypoints = new QButtonGroup(this);
	bgKeypoints->addButton(ui->rbNarf);
	bgKeypoints->addButton(ui->rbDense);
	bgKeypoints->addButton(ui->rbHarris3D);
	ui->rbHarris3D->setChecked(true);

	QButtonGroup *bgClassifier = new QButtonGroup(this);
	bgClassifier->addButton(ui->rbSVM);
	bgClassifier->addButton(ui->rbKNN);
	ui->rbSVM->setChecked(true);

	QButtonGroup *bgFeatures = new QButtonGroup(this);
	bgFeatures->addButton(ui->rbShot);
	bgFeatures->addButton(ui->rbShotColor);
	bgFeatures->addButton(ui->rbFPFH);
	bgFeatures->addButton(ui->rbPFHRGB);
	bgFeatures->addButton(ui->rbNarfFE);
	ui->rbFPFH->setChecked(true);

	QButtonGroup *bgSVMParams = new QButtonGroup(this);
	bgSVMParams->addButton(ui->rbLinear);
	bgSVMParams->addButton(ui->rbPoly);
	bgSVMParams->addButton(ui->rbRBF);
	bgSVMParams->addButton(ui->rbSigmoid);
	bgSVMParams->addButton(ui->rbPrecomputed);	
	ui->rbLinear->setChecked(true);

		// Connect "loadPCD" button and the function
	connect (ui->btnLoadPcd,  SIGNAL (clicked ()), this, SLOT (loadPcdPressed ()));

		// Connect "change training file" button and the function
	connect (ui->btnSelectTr,  SIGNAL (clicked ()), this, SLOT (changeTrainingFilePressed ()));

		// Connect "change test file" button and the function
	connect (ui->btnSelectTest,  SIGNAL (clicked ()), this, SLOT (changeTestFilePressed ()));

		// Connect "training" button and the function
	connect (ui->btnTraining,  SIGNAL (clicked ()), this, SLOT (trainingPressed ()));

		// Connect "test" button and the function
	connect (ui->btnTest,  SIGNAL (clicked ()), this, SLOT (testPressed ()));

		// Connect "extractKeypoints" button and the function
	connect (ui->btnExtractKeypoints,  SIGNAL (clicked ()), this, SLOT (extractKeypointsPressed ()));

	point_cloud_global.reset (new PointCloudType);
	
	viewerOriginalImage.reset (new pcl::visualization::PCLVisualizer ("Original Image", false));  
	ui->qvtkWidgetOriginal->SetRenderWindow (viewerOriginalImage->getRenderWindow ());

	viewerOriginalImage->setupInteractor (ui->qvtkWidgetOriginal->GetInteractor (), ui->qvtkWidgetOriginal->GetRenderWindow ());

	// Set up the QVTK window (extracted features)

	viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));  
	ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
	viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());

	ui->btnTraining->setEnabled(false);
	ui->btnTest->setEnabled(false);

	ui->btnExtractKeypoints->setEnabled(false);
	ui->sldAccuracy->setEnabled(false);
}

void SemanticLocalizationGUI::keypointsDetection(keyPointType type)
{	
	if(type==narf)
	{
		Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
		scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (point_cloud_global->sensor_origin_[0],point_cloud_global->sensor_origin_[1],point_cloud_global->sensor_origin_[2])) *	Eigen::Affine3f (point_cloud_global->sensor_orientation_);

		float noise_level = 0.0;
		float min_range = 0.0f;
		int border_size = 1;

		boost::shared_ptr<pcl::RangeImage> range_image_ptr(new pcl::RangeImage);
		pcl::RangeImage& range_image = *range_image_ptr;
	   
		range_image.createFromPointCloud (*point_cloud_global, angular_resolution, pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);

		// FEATURES EXTRACTION

		if (setUnseenToMaxRange)
			range_image.setUnseenToMaxRange ();

		// --------------------------------
		// -----Extract NARF keypoints-----
		// --------------------------------

		std::cout << "Point cloud size: "<<point_cloud_global->points.size ()<<".\n";
		std::cout << "Range image size: "<<range_image.points.size ()<<".\n";

		pcl::RangeImageBorderExtractor range_image_border_extractor;
		pcl::NarfKeypoint narf_keypoint_detector;
		narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
		narf_keypoint_detector.setRangeImage (&range_image);
		narf_keypoint_detector.getParameters ().support_size = support_size;

		pcl::PointCloud<int> selected_keypoints_global;
		narf_keypoint_detector.compute (selected_keypoints_global);
		std::cout << "Found "<<selected_keypoints_global.points.size ()<<" key points.\n";

		// -------------------------------------
		// -----Show keypoints in 3D viewer-----
		// -------------------------------------

		pcl::PointCloud<KeyPointType>::Ptr keypoints_ptr (new pcl::PointCloud<KeyPointType>);
		pcl::PointCloud<KeyPointType>& keypoints = *keypoints_ptr;
		keypoints.points.resize(selected_keypoints_global.points.size ());

		for (size_t i=0; i<selected_keypoints_global.points.size (); ++i)
			keypoints.points[i].getVector3fMap () = range_image.points[selected_keypoints_global.points[i]].getVector3fMap ();

		pcl::visualization::PointCloudColorHandlerCustom<KeyPointType> keypoints_color_handler (keypoints_ptr, 0, 255, 0);
		if(firstTimeExtraction)
		{
			viewer->addPointCloud<KeyPointType> (keypoints_ptr, keypoints_color_handler, "keypoints");
			firstTimeExtraction = false;
		}
		else
			viewer->updatePointCloud<KeyPointType> (keypoints_ptr, keypoints_color_handler, "keypoints");

		viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints"); 
	}
	else if(type==dense)
	{
		// ----- Uniform Sampling

		pcl::PointCloud<int>selected_keypoints_global;
		pcl::UniformSampling<PointType>uniform_sampling;
		uniform_sampling.setInputCloud(point_cloud_global);
		uniform_sampling.setRadiusSearch(0.05f); 
		uniform_sampling.compute (selected_keypoints_global);
		std::cout << "Found "<<selected_keypoints_global.points.size ()<<" key points.\n";

		point_cloud_sampled.reset (new PointCloudType);
		point_cloud_sampled->points.resize (selected_keypoints_global.points.size ());

		pcl::copyPointCloud (*point_cloud_global, selected_keypoints_global.points, *point_cloud_sampled);
		std::cout << "Model total points: " << point_cloud_global->size () << "; Selected Keypoints: " << point_cloud_sampled->size () << std::endl;

		pcl::visualization::PointCloudColorHandlerCustom<PointType> keypoints_color_handler (point_cloud_sampled, 255, 255, 0);

		if(firstTimeExtraction)
		{
			viewer->addPointCloud<PointType> (point_cloud_sampled, keypoints_color_handler, "keypoints");
			firstTimeExtraction = false;
		}
		else
			viewer->updatePointCloud<PointType> (point_cloud_sampled, keypoints_color_handler, "keypoints");

		viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "keypoints");
	}
	else if(type==iss)
	{
		double iss_salient_radius_;
		double iss_non_max_radius_;
		double iss_gamma_21_ (0.975);
		double iss_gamma_32_ (0.975);
		double iss_min_neighbors_ (5);

		int iss_threads_ (4);

		double model_resolution=0.2;

		iss_salient_radius_ = 6 * model_resolution;
		iss_non_max_radius_ = 4 * model_resolution;

		pcl::ISSKeypoint3D<PointType,PointType>iss_detector;
		iss_detector.setSalientRadius(iss_salient_radius_);
		iss_detector.setNonMaxRadius(iss_non_max_radius_);
		iss_detector.setInputCloud(point_cloud_global);
		iss_detector.compute(*point_cloud_sampled);

		pcl::visualization::PointCloudColorHandlerCustom<PointType> keypoints_color_handler (point_cloud_sampled, 255, 126, 126);

		if(firstTimeExtraction)
		{
			viewer->addPointCloud<PointType> (point_cloud_sampled, keypoints_color_handler, "keypoints");
			firstTimeExtraction = false;
		}
		else
			viewer->updatePointCloud<PointType> (point_cloud_sampled, keypoints_color_handler, "keypoints");

		viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "keypoints");

		std::cout << "Found "<<point_cloud_sampled->size ()<<" key points.\n";

	}
	else if(type==harris3d)
	{				
		pcl::HarrisKeypoint3D<PointType, pcl::PointXYZI, NormalType> harris3d;
		harris3d.setInputCloud (point_cloud_global);
		harris3d.setRadius (normal_radius); 
		harris3d.setRefine (false);
		harris3d.setNonMaxSupression (true);
		harris3d.setThreshold (0.01f);

		pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints(new pcl::PointCloud<pcl::PointXYZI>()); 
		harris3d.compute (*keypoints);

		point_cloud_sampled.reset (new PointCloudType);
		pcl::copyPointCloud(*keypoints, *point_cloud_sampled);	

		pcl::visualization::PointCloudColorHandlerCustom<PointType> keypoints_color_handler (point_cloud_sampled, 255, 126, 0);

		if(firstTimeExtraction)
		{
			viewer->addPointCloud<PointType> (point_cloud_sampled, keypoints_color_handler, "keypoints");
			firstTimeExtraction = false;
		}
		else
			viewer->updatePointCloud<PointType> (point_cloud_sampled, keypoints_color_handler, "keypoints");

		viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "keypoints");

		std::cout << "Found "<<point_cloud_sampled->size ()<<" key points.\n";
	}
}

void 
SemanticLocalizationGUI::updateCameraPosition (boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer, const Eigen::Affine3f& viewer_pose)
{
  Eigen::Vector3f pos_vector = viewer_pose * Eigen::Vector3f(0, 0, 0);
  Eigen::Vector3f look_at_vector = viewer_pose.rotation () * Eigen::Vector3f(0, 0, 1) + pos_vector;
  Eigen::Vector3f up_vector = viewer_pose.rotation () * Eigen::Vector3f(0, -1, 0);
  viewer->setCameraPosition (pos_vector[0], pos_vector[1], pos_vector[2],
                            look_at_vector[0], look_at_vector[1], look_at_vector[2],
                            up_vector[0], up_vector[1], up_vector[2]);
}

void SemanticLocalizationGUI::loadPclFileGlobal()
{
	pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
	pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;	

	pcl::PointCloud<pcl::PointWithViewpoint> far_ranges;
  	Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
	std::string filename = QFileDialog::getOpenFileName(this,tr("Open Image"), "../data/", tr("PCD Files (*.pcd)")).toStdString();
	if (pcl::io::loadPCDFile (filename, point_cloud) == -1)
		{
		cerr << "Was not able to open file \""<<filename<<"\".\n";
		}

	point_cloud_global = point_cloud_ptr;

	viewerOriginalImage->removeAllPointClouds();
	viewer->removeAllPointClouds();

	viewerOriginalImage->addPointCloud (point_cloud_global, "Original Image");
	viewerOriginalImage->resetCamera ();
	ui->qvtkWidgetOriginal->update ();

	viewer->addPointCloud (point_cloud_global, "Original Image");
	viewer->resetCamera ();
	ui->qvtkWidget->update ();

	scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (point_cloud_global->sensor_origin_[0],point_cloud_global->sensor_origin_[1],point_cloud_global->sensor_origin_[2])) *	Eigen::Affine3f (point_cloud_global->sensor_orientation_);

	updateCameraPosition(viewerOriginalImage, scene_sensor_pose);
	updateCameraPosition(viewer, scene_sensor_pose);

	firstTimeExtraction = true;
	firstTimeLBP = true;

	ui->btnExtractKeypoints->setEnabled(true);
}

// QT connections

void
SemanticLocalizationGUI::changeTrainingFilePressed()
{
	std::string filename = QFileDialog::getOpenFileName(this,tr("Open Configuration File"), "../data/", tr("Configuration Files (*.txt)")).toStdString();
	trainingConfigFile = filename;
	ui->lblTraining->setText(QString::fromStdString( filename).section('/',-1));

	ui->btnTraining->setEnabled(true);
}

void
SemanticLocalizationGUI::changeTestFilePressed()
{
	std::string filename = QFileDialog::getOpenFileName(this,tr("Open Configuration File"), "../data/", tr("Configuration Files (*.txt)")).toStdString();	
	testConfigFile = filename; 
	ui->lblTest->setText(QString::fromStdString( filename).section('/',-1));
	ui->btnTest->setEnabled(true);
}

void
SemanticLocalizationGUI::trainingPressed()
{	
	
	if(ui->rbSVM->isChecked())			// SVM 
	{
		semanticLocalizationSystem = new SemanticLocalizationBoWSVM();
		
		if(ui->rbLinear->isChecked())
			((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = LINEAR;
		else if(ui->rbPoly->isChecked())
			((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = POLY;
		else if(ui->rbRBF->isChecked())
			((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = RBF;
		else if(ui->rbSigmoid->isChecked())
			((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = SIGMOID;
		else if(ui->rbPrecomputed->isChecked())
			((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = PRECOMPUTED;

		((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.degree = ui->sldSVMDegree->value();
		((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.gamma = (double)(ui->sldSVMGamma->value())/10.0;
	}
	else if(ui->rbKNN->isChecked())			// kNN
	{
		semanticLocalizationSystem = new SemanticLocalizationBoWKnn();  
		((SemanticLocalizationBoWKnn*)semanticLocalizationSystem)->kValue = ui->sldkNNkValue->value();	
		
	}

	semanticLocalizationSystem->dataset_path = "../data/";

	// kEYPOINTS DETECTOR

	if(ui->rbNarf->isChecked())
		semanticLocalizationSystem->detector = new SemanticKeypointDetectorNARF();
	else if(ui->rbDense->isChecked())
		semanticLocalizationSystem->detector = new SemanticKeypointDetectorUniform();
	else if(ui->rbHarris3D->isChecked())
		semanticLocalizationSystem->detector =  new SemanticKeypointDetectorHarris3D();

	// FEATURES EXTRACTOR

	if(ui->rbShot->isChecked())
		semanticLocalizationSystem->extractor = new SemanticFeatureExtractorSHOT();
	else if(ui->rbShotColor->isChecked())
		semanticLocalizationSystem->extractor = new SemanticFeatureExtractorColorSHOT();
	else if(ui->rbFPFH->isChecked())
		semanticLocalizationSystem->extractor = new SemanticFeatureExtractorFPFH();
	else if(ui->rbPFHRGB->isChecked())
		semanticLocalizationSystem->extractor = new SemanticFeatureExtractorPFHRGB();
	else if(ui->rbNarfFE->isChecked())
		semanticLocalizationSystem->extractor = new SemanticFeatureExtractorNARF();

  	((SemanticLocalizationBoW*)semanticLocalizationSystem)->dictionarySize = ui->horizSliderDictSize->value();	
	semanticLocalizationSystem->readConfigurationTraining(trainingConfigFile);

	cout << "Data readed, frames: "<< semanticLocalizationSystem->framesTraining.size() << endl;
	cout << "Init train det:" << semanticLocalizationSystem->detector->name << " des:" << semanticLocalizationSystem->extractor->name << endl;
	semanticLocalizationSystem->train();
	cout << "End train" << endl;

	ui->btnTest->setEnabled(true);
}

void
SemanticLocalizationGUI::testPressed()
{
	if(testConfigFile=="")
		cout << "The test configuration should be established" << endl;
	else
		{		
		semanticLocalizationSystem->readConfigurationTest(testConfigFile);
		cout << "Data readed, frames: "<< semanticLocalizationSystem->framesTest.size() << endl;
		double accuracy = semanticLocalizationSystem->test();
		ui->sldAccuracy->setValue((int)(100*accuracy));
		cout << "End test" << endl;
		}
	ui->btnTest->setEnabled(false);	// To avoid training/test with different configurations
}

void
SemanticLocalizationGUI::loadPcdPressed()
{
	loadPclFileGlobal();	
}


void
SemanticLocalizationGUI::extractKeypointsPressed()
{
	if(ui->rbNarf->isChecked())
		keypointsDetection(narf);
	else if(ui->rbDense->isChecked())
		keypointsDetection(dense);
	else if(ui->rbHarris3D->isChecked())
		keypointsDetection(harris3d);
}

SemanticLocalizationGUI::~SemanticLocalizationGUI ()
{
  delete ui;
}
