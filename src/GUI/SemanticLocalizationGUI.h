 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#ifndef SEMANTICLOCALIZATIONGUI_H
#define SEMANTICLOCALIZATIONGUI_H

#include <iostream>
#include <sstream>
#include <vector>

// Qt
#include <QMainWindow>
#include <QFileDialog>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

#include "SemanticKeypointDetectorUniform.h"
#include "SemanticKeypointDetectorHarris3D.h"
#include "SemanticKeypointDetectorNARF.h"

#include "SemanticFeatureExtractorSHOT.h"
#include "SemanticFeatureExtractorColorSHOT.h"
#include "SemanticFeatureExtractorFPFH.h"
#include "SemanticFeatureExtractorPFHRGB.h"
#include "SemanticFeatureExtractorNARF.h"

#include "SemanticLocalization.h"
#include "SemanticLocalizationBoWSVM.h"
#include "SemanticLocalizationBoWKnn.h"

typedef pcl::PointXYZRGB PointType;
typedef pcl::PointXYZ KeyPointType;
typedef pcl::PointCloud<PointType> PointCloudType;


using namespace pcl;
using namespace std;

enum keyPointType { 
	narf,
	dense, 
	iss, 
	harris3d
};



namespace Ui
{
  class SemanticLocalizationGUI;
}

class SemanticLocalizationGUI : public QMainWindow
{
  Q_OBJECT

public:
  explicit SemanticLocalizationGUI (QWidget *parent = 0);
  void loadPclFileGlobal();

  void keypointsDetection(keyPointType type);

  void updateCameraPosition (boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer, const Eigen::Affine3f& viewer_pose);

  ~SemanticLocalizationGUI ();


public slots:

  void 
  loadPcdPressed();

  void 
  trainingPressed();

  void
  testPressed();

  void 
  changeTrainingFilePressed();

  void
  changeTestFilePressed();

  void 
  extractKeypointsPressed();

protected:

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerOriginalImage;

  SemanticLocalization* semanticLocalizationSystem; 

  string trainingConfigFile;
  string testConfigFile;

  PointCloudType::Ptr cloud;
  PointCloudType::Ptr cloudType;
  
  PointCloudType::Ptr point_cloud_global;
  PointCloudType::Ptr point_cloud_sampled;

  pcl::PointCloud<int> selected_keypoints_global;

private:
  Ui::SemanticLocalizationGUI *ui;

};

#endif
