 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "SemanticLocalization.h"

#include <string>

namespace fs = ::boost::filesystem;
using namespace std;

std::vector<std::string> FrameItem::classes;
std::vector<std::string> FrameItem::object_names;

SemanticLocalization::SemanticLocalization()
{
	dataset_path = "";
	pcl::console::setVerbosityLevel(pcl::console::L_ERROR);
}

#define VERBOSE

/*
*	Reads the configuration file and leaves the information in the frames object 
*
*/

void SemanticLocalization::readConfigurationTraining(fs::path conf_path)
{
	ifstream myReadFile;
	myReadFile.open(conf_path.string().c_str());
	string aux,color,depth;
	int iclass;
	int numberOfItems;
	framesTraining.resize(0);

	int lineiter = 0;
	if (myReadFile.is_open()) 
		{

		//first line is the number of Items
		myReadFile >> numberOfItems;
		getline(myReadFile, aux); //discard rest of the first line

		//second is the classes names
		getline(myReadFile, aux);
		boost::split(FrameItem::classes, aux, boost::is_any_of("\t "), boost::token_compress_on);

		//third is the objects names
		getline(myReadFile, aux);
		boost::split(FrameItem::object_names, aux, boost::is_any_of("\t "), boost::token_compress_on);

		//color depth class objectsappearances 
		while (!myReadFile.eof()) 
		{			
			myReadFile >> color;
			myReadFile >> depth;
			myReadFile >> iclass;
			FrameItem frame(color, depth, iclass);
			if(getline(myReadFile, aux))
			{
				stringstream lineStream(aux);
				boost::shared_ptr<std::vector<int> > obappe = boost::shared_ptr<std::vector<int> >(new std::vector<int>());
				copy(istream_iterator<int>(lineStream), istream_iterator<int>(), back_inserter(*obappe));
				frame.object_appearances = obappe;
				framesTraining.push_back(frame);
				lineiter++;
			}
		}
	}

	numClases = FrameItem::classes.size();
	
	myReadFile.close();
}


/*
*	Reads the configuration file and leaves the information in the frames object 
*
*/

void SemanticLocalization::readConfigurationTest(fs::path conf_path)
{
	ifstream myReadFile;
	myReadFile.open(conf_path.string().c_str());
	string aux,color,depth;
	int iclass;
	int numberOfItems;
	framesTest.resize(0);

	int lineiter = 0;
	if (myReadFile.is_open()) 
		{

		//first line is the number of Items
		myReadFile >> numberOfItems;
		getline(myReadFile, aux); //discard rest of the first line

		//second is the classes names
		getline(myReadFile, aux);
		boost::split(FrameItem::classes, aux, boost::is_any_of("\t "), boost::token_compress_on);

		//third is the objects names
		getline(myReadFile, aux);
		boost::split(FrameItem::object_names, aux, boost::is_any_of("\t "), boost::token_compress_on);

		//color depth class objectsappearances 
		while (!myReadFile.eof()) 
		{			
			myReadFile >> color;
			myReadFile >> depth;
			myReadFile >> iclass;
			FrameItem frame(color, depth, iclass);
			if(getline(myReadFile, aux))
			{
				stringstream lineStream(aux);
				boost::shared_ptr<std::vector<int> > obappe = boost::shared_ptr<std::vector<int> >(new std::vector<int>());
				copy(istream_iterator<int>(lineStream), istream_iterator<int>(), back_inserter(*obappe));
				frame.object_appearances = obappe;
				framesTest.push_back(frame);
				lineiter++;
			}
		}
	}

	myReadFile.close();
}
void SemanticLocalization::validate()
{
}

/*
*	From the current frames object (previously loaded through the readConfiguration function), 
*	the classifyFrameScene() function is called to get some stats
*
*/

double SemanticLocalization::test()
{
	string depth_path_file,key_path_file, feat_path_file,frame_name;
	std::vector<std::vector<float*> > featuresFromFiles;
	std::vector<int> testClasses;
	std::vector<float*> features;

	PointCloud<PointXYZRGB>::Ptr points(new PointCloud<PointXYZRGB>());
	PointCloud<PointXYZRGB>::Ptr keypoints(new PointCloud<PointXYZRGB>());

	featuresFromFiles.resize(0);
	testClasses.resize(0);

	/*
	* 	For each test pcd file, load/extract all the features from the keypoint/features combination
	*/

	std::cout << std::endl << std::endl << " TEST STAGE " << std::endl << std::endl;	

	for (std::vector<FrameItem>::iterator it = framesTest.begin(); it != framesTest.end(); ++it)
	{
		for (std::vector<float*>::iterator itb = features.begin(); itb != features.end(); ++itb)
			free(*itb);

		features.resize(0);
		depth_path_file = (dataset_path / (*it).depth_path).string();
		frame_name = depth_path_file;
		frame_name.erase(frame_name.find_last_of("."), string::npos);
		key_path_file = frame_name + "_" + detector->name + ".pcd";
		feat_path_file = frame_name + "_" + detector->name + "_" + extractor->name + +".pcd";

		try
		{
			if (boost::filesystem::exists(feat_path_file))
			{
				// Case A.- A file with the features previously extracted exists --> load it
				extractor->loadFeatures(feat_path_file, features);

			#ifdef VERBOSE
				std::cout << "Features from " << (*it).depth_path << " loaded : " << features.size() << std::endl;
			#endif
			}
			else{
				pcl::io::loadPCDFile(depth_path_file, *points);
				if (boost::filesystem::exists(key_path_file))
				{
					// Case B.- A file with the keypoints previously detection exists --> load it
					io::loadPCDFile(key_path_file, *keypoints);
				}
				else
				{
					// Case C.- No files --> keypoints detection
					detector->detectKeypoints(points, keypoints);
					io::savePCDFile(key_path_file, *keypoints);

				}
				
				// Extract features and save them into a file

				extractor->extractFeatures(points, keypoints, features);
				extractor->saveFeatures(feat_path_file, features);
				#ifdef VERBOSE
					std::cout << "Features from " << (*it).depth_path << " computed : " << features.size() << std::endl;
				#endif		
	
				//release memory from points/keypoints
				points->clear();
				keypoints->clear();
			}
		}
		catch (std::exception e)
		{
			std::cout << "Exception in  " << (*it).depth_path << " : " << e.what()<< std::endl;
		}
		
		// Add the features extracted from the file i to the all features collection

		std::vector<float*> featuresToClone;		
		for (int i = 0; i < features.size(); i++)
		{
			float *feat = new float[extractor->dimensionality];
			for (int j = 0; j < extractor->dimensionality; j++)
			{
				feat[j] = (float)features.at(i)[j];
			}
			featuresToClone.push_back(feat);
		}		

		featuresFromFiles.push_back(featuresToClone);

		// Add the semantic class of the test
		testClasses.push_back((*it).item_class);			
	}

	/*
	*	CLASSIFICATION
	*
	* 	INPUT --> featuresFromFiles, testClasses, 
	*		
	*/

	float numFails = 0.0;
	float numHits = 0.0;

	std::cout << endl;

	for(int i=0;i<featuresFromFiles.size();i++)
	{
		int retvalBoW = classifyFrameScene(featuresFromFiles.at(i));
		if(testClasses.at(i)==retvalBoW)
			numHits+= 1.0;
		else
			numFails+= 1.0;

		std::cout << "Test frame " << i << " - Real Scene: " << testClasses.at(i) << " - Estimated Scene: " << retvalBoW << std::endl;
	}

	printf("Percentage of hits:%2.2f and fails :%2.2f\n",100.0*(numHits/(numHits+numFails)), 100.0*(numFails/(numHits+numFails)));

	std::cout << endl;

	return (double)(numHits/(numHits+numFails));
}

void 
SemanticLocalization::saveCSVFile(std::vector<float*> &featuresToSave, std::vector<int> &classesToSave, string fileName, int featuresLength)
{
	ofstream csvFile;
	csvFile.open (fileName.c_str());

	for(int i=0;i<featuresToSave.size();i++)
		{	
		for(int j=0;j<featuresLength;j++)
			{	
			csvFile << featuresToSave.at(i)[j] << ",";				
			}
		csvFile << classesToSave.at(i) << "\n";
	}
	csvFile.close();
}

void 
SemanticLocalization::loadCSVFile(std::vector<float*> &featuresToLoad, std::vector<int> &classesToLoad, string fileName)
{
	ifstream csvFile;
	csvFile.open (fileName.c_str());
	string line;
	featuresToLoad.resize(0);
	classesToLoad.resize(0);
	while(getline(csvFile,line))
	{
		std::vector<string> strs;
		boost::split(strs, line, boost::is_any_of(","));
		float *desc = new float[strs.size()-1];

		for (int j = 0; j < strs.size()-1; j++)
		{
			desc[j] = boost::lexical_cast<float>(strs.at(j)); 
		}
		featuresToLoad.push_back(desc);
		classesToLoad.push_back(boost::lexical_cast<int>(strs.at(strs.size()-1)));
	}

	csvFile.close();
}

SemanticLocalization::~SemanticLocalization()
{
}
