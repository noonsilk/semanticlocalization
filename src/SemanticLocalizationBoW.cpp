 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "SemanticLocalizationBoW.h"

namespace fs = ::boost::filesystem;
using namespace std;


SemanticLocalizationBoW::SemanticLocalizationBoW()
{

	// Max number of features as input for the k-means

	maxTrainingFeatures = 100000;
}

#define VERBOSE

void
SemanticLocalizationBoW::trainingWordAssignation()
{
	// From each training image, we compute its features and obtain
	// a trainig word based on the dictionary

	string depth_path_file,key_path_file, feat_path_file,frame_name, dictionary_name;

	PointCloud<PointXYZRGB>::Ptr points(new PointCloud<PointXYZRGB>());
	PointCloud<PointXYZRGB>::Ptr keypoints(new PointCloud<PointXYZRGB>());

	std::vector<float*> features;

	bool errorInExtraction=false;

	trainingClasses.resize(0);
	trainingWords.resize(0);

	trainingClasses.reserve(framesTraining.size());
	trainingWords.reserve(framesTraining.size());

	for (std::vector<FrameItem>::iterator it = framesTraining.begin(); it != framesTraining.end(); ++it)
	{
		errorInExtraction=false;

		depth_path_file = (dataset_path / (*it).depth_path).string();
		frame_name = depth_path_file;
		frame_name.erase(frame_name.find_last_of("."), string::npos);
		key_path_file = frame_name + "_" + detector->name + ".pcd";
		feat_path_file = frame_name + "_" + detector->name + "_" + extractor->name + +".pcd";

		features.resize(0);

		try
		{
			if (boost::filesystem::exists(feat_path_file))
			{
				// Case A.- A file with the features previously extracted exists --> load it
				extractor->loadFeatures(feat_path_file, features);

			#ifdef VERBOSE
				std::cout << "Features from " << (*it).depth_path << " loaded : " << features.size() << std::endl;
			#endif
			}
			else
			{
				//std::cout << "File not found ... temporary exit "<< std::endl;
				//errorInExtraction = true;

				pcl::io::loadPCDFile(depth_path_file, *points);
				if (boost::filesystem::exists(key_path_file))
				{
					// Case B.- A file with the keypoints previously detection exists --> load it
					io::loadPCDFile(key_path_file, *keypoints);
				}
				else
				{
					// Case C.- No files --> keypoints detection
					detector->detectKeypoints(points, keypoints);
					io::savePCDFile(key_path_file, *keypoints);
				}
				
			// Extract features and save them into a file

			extractor->extractFeatures(points, keypoints, features);
			extractor->saveFeatures(feat_path_file, features);
			#ifdef VERBOSE
				std::cout << "Features from " << (*it).depth_path << " computed : " << features.size() << std::endl;
			#endif		

			//release memory from points/keypoints
			points->clear();
			keypoints->clear();
			}
		}
		catch (std::exception e)
		{
			std::cout << "Exception in  " << (*it).depth_path << " : " << e.what()<< std::endl;
			errorInExtraction = true;
		}		

		if(!errorInExtraction)
		{
			// Assign the extracted features to a training word and store it
			trainingWords.push_back(wordsAssignation(features));			

			// Add the semantic class
			trainingClasses.push_back((*it).item_class);	
		}

		// Free memory

		for (std::vector<float*>::iterator itb = features.begin(); itb != features.end(); ++itb)
			free(*itb);

		features.resize(0);				
	}	
}
void 
SemanticLocalizationBoW::generateTrainingWords(bool saveCSV)
{
	string dictionary_name;
	dictionary_name = "dictionary_" + detector->name + "_" + extractor->name + + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".pcd";
	dictionary.resize(0);

	if (!boost::filesystem::exists(dictionary_name))	
		generateDictionaryFromTraining();		// The dictionary does not exist --> generate and save
	else
		extractor->loadFeatures(dictionary_name, dictionary);		
		
	// Each centroid of the dictionary corresponds with a local feature, then we use the same method for load/save the dictionary

	#ifdef VERBOSE
		std::cout  << std::endl << "Dictionary with size " << dictionarySize << " loaded from file " << dictionary_name << std::endl  << std::endl;
	#endif

	trainingWordAssignation();

	if(saveCSV)
	{
		string fileName = "training_"+detector->name + "_" + extractor->name + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".csv";
		saveCSVFile(trainingWords, trainingClasses, fileName, dictionarySize);
	}
}

float* 
SemanticLocalizationBoW::wordsAssignation(std::vector<float*>  &featuresFromFile)
{
	float minDistance = std::numeric_limits<float>::max();
	int clusterId = 0;
	int selectedClusterId = 0;
	float dist;	
	Kmeans distCalculator(1, extractor->dimensionality);

	// For each feature, assign it to its closest word/centroid
		
	float *wordOccurrence = new float[dictionarySize];
	for(int i=0;i<dictionarySize;i++)	
		wordOccurrence[i]=0;

	for(int i=0;i<featuresFromFile.size();i++)
	{
		minDistance = std::numeric_limits<float>::max();
		std::vector<float> featuresAtKeypoint;
		std::vector<float> centroid;
		
		centroid.reserve(extractor->dimensionality);

		clusterId = 0;

		// Generate a vector with the features extracted at point i

		for(int z=0;z<extractor->dimensionality;z++)			
			featuresAtKeypoint.push_back((float)featuresFromFile.at(i)[z]);

		// For each word/centroid, generate a vector with its features

		for(int m=0;m<dictionary.size();m++)			
		{
			centroid.resize(0);
			featuresAtKeypoint.reserve(extractor->dimensionality);

			for(int z=0;z<extractor->dimensionality;z++)				
				centroid.push_back((float)dictionary.at(m)[z]);	
	
			dist =  distCalculator.distance(centroid, featuresAtKeypoint);	

			if(dist<minDistance)
			{
				minDistance = dist;
				selectedClusterId = clusterId;
			}

			clusterId++;	
		}			

		// Increase the occurrences for such bin
		wordOccurrence[selectedClusterId]=wordOccurrence[selectedClusterId]+1;
	}

	for(int i=0;i<dictionarySize;i++)		
		wordOccurrence[i]=wordOccurrence[i]/featuresFromFile.size();		

	return wordOccurrence;
}


void SemanticLocalizationBoW::computeDictionary(std::vector<float*> &featuresInput, std::vector<float*> &dictionary)
{
	dictionary.resize(0);
	dictionary.reserve(dictionarySize);
	
	std::cout << "featuresInput.size():" << featuresInput.size() << std::endl;
	

	// Kmeans clustering from all features
	
	// 1- Initialize the kMeans with the number of entries and dimensionality

	Kmeans dictionaryGenerator((int)featuresInput.size(), extractor->dimensionality);


	// 2- Set the value for k
	
	dictionaryGenerator.setClusterSize(dictionarySize);

	// 3- Add points for the clustering

	for(int i=0;i<featuresInput.size();i++)
	{

		std::vector<float> featuresAtKeypoint;
		featuresAtKeypoint.reserve(extractor->dimensionality);

		for(int j=0;j<extractor->dimensionality;j++)		
			featuresAtKeypoint.push_back(featuresInput.at(i)[j]);
		
		dictionaryGenerator.addDataPoint(featuresAtKeypoint);
		std::vector<float>().swap(featuresAtKeypoint);	
	}

	// 4- clustering process

	std::cout << "Starting kMeans process..."<< std::endl;
	dictionaryGenerator.kMeans();
	std::cout << "... end of kMeans process"<< std::endl;
	Kmeans::Centroids centroids = dictionaryGenerator.get_centroids();

	std::cout << "C "<< std::endl;

	// 5 - store the centroids as dictionary
	
	BOOST_FOREACH(Kmeans::Centroids::value_type c, centroids)
	{
		float *desc = new float[extractor->dimensionality];
		for (int j = 0; j < extractor->dimensionality; j++)		
			desc[j] = (float)c.at(j);		
		dictionary.push_back(desc);
	}
}

void 
SemanticLocalizationBoW::generateDictionaryFromTraining()
{
	// From each training image, we compute its features and perform
	// a random shufle to obtain an appropriate number of features per image
	// to generate a final set of 100.000 training features that will be used
	// to compute the dictionary using a k-means procedure 

	string depth_path_file,key_path_file, feat_path_file,frame_name, dictionary_name;

	PointCloud<PointXYZRGB>::Ptr points(new PointCloud<PointXYZRGB>());
	PointCloud<PointXYZRGB>::Ptr keypoints(new PointCloud<PointXYZRGB>());

	std::vector<float*> allFeatures;
	std::vector<float*> features;
	int featuresPerImage = 	maxTrainingFeatures / framesTraining.size();
	std::cout << std::endl << "Features per image " << featuresPerImage << std::endl;

	bool errorInExtraction=false;
	
	allFeatures.resize(0);	
	allFeatures.reserve(featuresPerImage);	

	for (std::vector<FrameItem>::iterator it = framesTraining.begin(); it != framesTraining.end(); ++it)
	{
		errorInExtraction=false;

		depth_path_file = (dataset_path / (*it).depth_path).string();
		frame_name = depth_path_file;
		frame_name.erase(frame_name.find_last_of("."), string::npos);
		key_path_file = frame_name + "_" + detector->name + ".pcd";
		feat_path_file = frame_name + "_" + detector->name + "_" + extractor->name + +".pcd";

		try
		{
			if (boost::filesystem::exists(feat_path_file))
			{
				// Case A.- A file with the features previously extracted exists --> load it
				extractor->loadFeatures(feat_path_file, features);

			#ifdef VERBOSE
				std::cout << "Features from " << (*it).depth_path << " loaded : " << features.size() << std::endl;
			#endif
			}
			else
			{
				pcl::io::loadPCDFile(depth_path_file, *points);
				if (boost::filesystem::exists(key_path_file))
				{
					// Case B.- A file with the keypoints previously detection exists --> load it
					io::loadPCDFile(key_path_file, *keypoints);
				}
				else
				{
					// Case C.- No files --> keypoints detection
					detector->detectKeypoints(points, keypoints);
					io::savePCDFile(key_path_file, *keypoints);
				}
				
			// Extract features and save them into a file
			extractor->extractFeatures(points, keypoints, features);
			extractor->saveFeatures(feat_path_file, features);
			#ifdef VERBOSE
				std::cout << "Features from " << (*it).depth_path << " computed : " << features.size() << std::endl;
			#endif		

			//release memory from points/keypoints
			points->clear();
			keypoints->clear();
			}
		}
		catch (std::exception e)
		{
			std::cout << "Exception in  " << (*it).depth_path << " : " << e.what()<< std::endl;
			errorInExtraction = true;
		}
		
		// Add the features extracted from the file i to the all features 
		//	collection only when no errors are present

		if(!errorInExtraction)
		{
			// Random shuffle for the extracted features			

			std::random_shuffle ( features.begin(), features.end() );

			std::cout << "Nº features to add  " << min(featuresPerImage,(int)features.size()) << std::endl;			

			// Select the featuresPerImage first features			
		
			for (int i = 0; i < min(featuresPerImage,(int)features.size()); i++)
			{
				float *feat = new float[extractor->dimensionality];
				for (int j = 0; j < extractor->dimensionality; j++)
				{
					feat[j] = (float)features.at(i)[j];
				}
				allFeatures.push_back(feat);
			}		
		}

		// Free memory

		for (std::vector<float*>::iterator itb = features.begin(); itb != features.end(); ++itb)
			free(*itb);

		features.resize(0);					
	}

	dictionary_name = "dictionary_" + detector->name + "_" + extractor->name + + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".pcd";	

	std::cout << "computeDictionary(allFeatures, dictionary) "<< std::endl;
	std::cout << "allFeatures.size():" << allFeatures.size() << std::endl;

	computeDictionary(allFeatures, dictionary);
	// Each centroid of the dictionary corresponds with a local feature, then we use the same method for load/save the dictionary
	extractor->saveFeatures(dictionary_name, dictionary);	
	
	for (std::vector<float*>::iterator itb = allFeatures.begin(); itb != allFeatures.end(); ++itb)
		free(*itb);

	allFeatures.resize(0);

	#ifdef VERBOSE
		std::cout << "Dictionary with size " << dictionarySize << " computed and saved as " << dictionary_name << std::endl;
	#endif
}

SemanticLocalizationBoW::~SemanticLocalizationBoW()
{
}
