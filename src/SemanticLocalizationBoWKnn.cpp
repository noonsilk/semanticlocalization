 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "SemanticLocalizationBoWKnn.h"
#include <flann/flann.hpp>
#include <boost/random.hpp>
#include <iostream>
#include <iomanip>

using namespace boost; 


namespace fs = ::boost::filesystem;
using namespace std;


SemanticLocalizationBoWKnn::SemanticLocalizationBoWKnn()
{
	kValue = 7;
}

#define VERBOSE

int SemanticLocalizationBoWKnn::classifyFrameScene(std::vector<float*> &features)
{
	std::vector<float*> testWords;
	int sceneLabel = 0;

	testWords.push_back(wordsAssignation(features));

	flann::Matrix<float> p = flann::Matrix<float>(new float[dictionarySize], 1, dictionarySize);
  	memcpy (&p.ptr ()[0], &testWords.at(0)[0], p.cols * p.rows * sizeof (float));
	
	flann::Matrix<int> indices;
	flann::Matrix<float> distances;
  	indices = flann::Matrix<int>(new int[kValue], 1, kValue);
  	distances = flann::Matrix<float>(new float[kValue], 1, kValue);

  	index->knnSearch (p, indices, distances, kValue, flann::SearchParams (512));

	int *appearances= new int[numClases];

	for(int i=0;i<numClases;i++)
		appearances[i] = 0;

	for (int i = 0; i < kValue; ++i)		
		appearances[trainingClasses[indices[0][i]]-1]++;		

	int max = -1;
	int indexMax = -1;
	for(int i=0;i<numClases;i++)
		{
		if(appearances[i]>max)
			{
			max = appearances[i];
			indexMax = i;
			}
		}

    	sceneLabel = indexMax+1;   	
	
	return sceneLabel;
}

void SemanticLocalizationBoWKnn::train()
{
	string fileName = "training_"+detector->name + "_" + extractor->name + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".csv";
	if(!boost::filesystem::exists(fileName))
		generateTrainingWords(true);
	else
		{

		std::cout << "Loading training words ..."<< std::endl;
		loadCSVFile(trainingWords, trainingClasses, fileName);

		// Regardless the training words, we need a dictionary to assign the features extracted from test files to test words
		dictionary.resize(0);

		string dictionary_name;
		dictionary_name = "dictionary_" + detector->name + "_" + extractor->name + + "_size_"+ boost::lexical_cast<std::string>(dictionarySize) + ".pcd";

		if (!boost::filesystem::exists(dictionary_name))	
			generateDictionaryFromTraining();		// The dictionary does not exist --> generate and save
		else
			extractor->loadFeatures(dictionary_name, dictionary);

		// Each centroid of the dictionary corresponds with a local feature, then we use the same method for load/save the dictionary
		}

	flann::Matrix<float> data (new float[trainingWords.size() * dictionarySize], trainingWords.size(), dictionarySize);
	for (size_t i = 0; i < data.rows; ++i)
	    for (size_t j = 0; j < data.cols; ++j)
	      data[i][j] = trainingWords.at(i)[j];

  	//index = new flann::Index<flann::ChiSquareDistance<float> >(data, flann::LinearIndexParams ());
  	index = new flann::Index<flann::ChiSquareDistance<float> >(data, flann::KDTreeIndexParams (4));
  	index->buildIndex ();	
}

SemanticLocalizationBoWKnn::~SemanticLocalizationBoWKnn()
{
}
